from adafruit_servokit import ServoKit
import time

kit = ServoKit(channels=16)

kit.servo[0].angle = 180
time.sleep(1)

while True:
    for i in range(180, 90, -3):
        print(i)
        kit.servo[0].angle = i
        time.sleep(0.01)

    time.sleep(0.5)

    for i in range(90, 180, 3):
        print(i)
        kit.servo[0].angle = i
        time.sleep(0.01)

    time.sleep(0.5)


