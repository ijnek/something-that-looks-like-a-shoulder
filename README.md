# Something that looks like a Shoulder

## Video

[Video](https://www.youtube.com/watch?v=g0A2d3KKRMI) of moving motor using ROS using code from the [move-motor-remotely](https://gitlab.com/ijnek/remote-control-motor-using-ros) project.

## Equipment

- HiTEC HS-311 Servo Motor
- Custom 3d printed chassis and gears (printed using ELEGOO MARS)
- Power Source
- Raspberry Pi 3b+
- PCA9685 16-Channel PWM Driver


## Previous versions
### Version 0.0.1
![](photo.png)
[Video](https://www.youtube.com/watch?v=FLT9bU7QFUk) of move-from-180-to-90-and-back.py

### Version 0.0.2
![](photo2.jpg)
